console.log("Hello Universe");

let number = parseInt(prompt("Enter a Number: "));
console.log("The number you provided is " + number);

	function loopNum() {
		for (let i = number; i >= 0; i--) {

			if (i <= 50) {
				console.log("The current value is at "+ i + ". Terminating the loop.")
				break;
			}

			if (i % 10 == 0) {
				console.log("The number is divisible by 10. Skipping the number.");
			}

			else if (i % 5 == 0) {
				console.log(i);
			}
		}
	}

	loopNum();

	let myString = "supercalifragilisticexpialidocious";
	let myConsonants = "";

	function loopString() {
		for (let i = 0; i < myString.length; i++) {
			if (myString[i] == "a" || myString[i] == "e" || myString[i] == "i" || myString[i] == "o" || myString[i] == "u"){
				continue;
			} else {

				myConsonants += myString[i]; 
			}
		}
	}

	loopString();
	console.log(myString);
	console.log(myConsonants);